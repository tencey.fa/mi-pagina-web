﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace ConstruCiudad
{
    public partial class Empleado : Form
    {
        public Empleado()
        {
            InitializeComponent();
        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection("Data Source=FR002S32;Initial Catalog=construcivil;Integrated Security=True;");

            if (TxbNombre.Text == "")
            {
                MessageBox.Show("el campo nombre esta  vacion");
                TxbNombre.Focus();


            }
            else if (TxbApellido.Text == "")
            {
                MessageBox.Show("el campo apellido esta vacio");
                TxbApellido.Focus();
            }
            else if (TxbDireccion.Text == "")
            {
                MessageBox.Show("el campo direccion esta vacio");
                TxbDireccion.Focus();
            }
            else if (TxbN_Documento.Text == "")
            {
                MessageBox.Show("el campo numero de documento esta vacio");
                TxbN_Documento.Focus();
            }
            else if (TxbCargo.Text == "")
            {
                MessageBox.Show("el campo cargo esta vacio");
                TxbCargo.Focus();
            }
            else if (TxbTelefono.Text == "")
            {
                MessageBox.Show("el campo telefono documento esta vacio");
                TxbTelefono.Focus(); 
            }
            else if (TxbId_proyecto.Text == "")
            {
                MessageBox.Show("el campo id_proyecto  esta vacio");
                TxbId_proyecto.Focus();
            }

            else
            {


                SqlCommand cmd = new SqlCommand("insert into reporte(@nombre,@apellido,@direccion,@numerodocumento,@cargo,@telefono,@id_proyecto)Values('" + TxbNombre.Text + "','" + TxbApellido.Text + "','" + TxbDireccion.Text + "', '" + TxbN_Documento.Text + "', '" + TxbCargo.Text + "', '" + TxbTelefono.Text + "', '" + TxbId_proyecto.Text + "',  ", con);
                con.Open();
                cmd.ExecuteNonQuery();
                MessageBox.Show("Se   agrego con exito ");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection("Data Source=FR002S32;Initial Catalog=construcivil;Integrated Security=True;");

            SqlCommand cmd = new SqlCommand("update empleado set nombre ='" + TxbNombre.Text + "', apellido = '" + TxbApellido.Text + "', direccion = '" + TxbDireccion.Text + "',  cargo = '" + TxbCargo.Text + "', telefono = '" + TxbTelefono.Text + "', id_proyecto = '" + TxbId_proyecto.Text + "' where numerodocumento = '" + TxbN_Documento.Text + "'", con);
            con.Open();
            cmd.ExecuteNonQuery();  
        }

        private void button3_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection("Data Source=FR002S32;Initial Catalog=construcivil;Integrated Security=True;");
            try
            {
                SqlCommand comando = new SqlCommand("insert into empleado values (@nombre,@apellido,@direccion,@numerodocumento,@cargo,@telefono,@id_proyecto)", con);
                comando.Parameters.AddWithValue("nombre", TxbNombre.Text);
                comando.Parameters.AddWithValue("apellido", TxbApellido.Text);
                comando.Parameters.AddWithValue("direccion", TxbDireccion.Text);
                comando.Parameters.AddWithValue("numerodocumento", TxbN_Documento.Text);
                comando.Parameters.AddWithValue("cargo", TxbCargo.Text);
                comando.Parameters.AddWithValue("telefono", TxbTelefono.Text);
                comando.Parameters.AddWithValue("id_proyecto", TxbId_proyecto.Text);
                comando.ExecuteNonQuery();
                MessageBox.Show("Datos eliminados");

            }
            catch
            {
                MessageBox.Show("Error");
            }
        }

        public void cargarGrid()
        {
            SqlConnection con = new SqlConnection("Data Source=FR002S32;Initial Catalog=construcivil;Integrated Security=True;");

            DataTable dt = new DataTable();
            SqlCommand consultar = new SqlCommand("SELECT * FROM empleado", con);
            SqlDataAdapter da = new SqlDataAdapter(consultar);


            da.Fill(dt);

            consultar.Connection = con;
            dataGridView1.DataSource = dt;
            consultar.ExecuteNonQuery();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnection con = new SqlConnection("Data Source=FR002S32;Initial Catalog=construcivil;Integrated Security=True;");

                DataTable dt = new DataTable();
                SqlCommand consultar = new SqlCommand("SELECT * FROM empleado", con);
                SqlDataAdapter da = new SqlDataAdapter(consultar);
                da.Fill(dt);
                consultar.Connection = con;
                dataGridView1.DataSource = dt;
                consultar.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Empleado_Load(object sender, EventArgs e)
        {

        }
    }
}
